import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator , ImageBackground , ScrollView , TouchableOpacity} from 'react-native';
import Axios from 'axios';
import Gap from '../component/Gap';
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      food:{},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getNewFood()
    this.getFood()
  }

  //   Get Api Users
  getNewFood = async () => {
    try {
      const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?f=c`)
      this.setState({ isError: false, isLoading: false, data: response.data })
      console.log("wertyui", response)
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }
  
  getFood= async () => {
    try {
      const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/categories.php`)
      this.setState({ isError: false, isLoading: false, food: response.data })
      console.log("wertyui" + response)
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <View style={styles.container}>
        <View style={{padding : 20}}>
          <ScrollView showsVerticalScrollIndicator={false}>
          <TouchableOpacity style={styles.header} onPress={() => this.props.navigation.navigate('About')}>
        <Image source={require('../images/ade.png')} style={styles.img} />
        <View style={styles.flex}>
        <Text style={styles.desc}>Hi, Welcome back</Text>
        <Text style={styles.name}>{this.props.route.params.username}</Text>
				</View>

			</TouchableOpacity>
          <Text style={styles.textcategory}>Food Category</Text>
          <Gap height={30}/>
          <View style={{justifyContent: 'space-between'}}>
            <FlatList 
                  data={this.state.food.categories}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({ item }) =>
                    <TouchableOpacity onPress={() => 
                  navigate('DetailCate' ,{
                    idCategory : item.idCategory ,
                    strCategory : item.strCategory,
                    strCategoryThumb : item.strCategoryThumb,
                    strCategoryDescription : item.strCategoryDescription,
                    })}>
                    <View style={{flexDirection: 'column'}}>
                    <Image source={{ uri: `${item.strCategoryThumb}` }} style={styles.Image}/>
                    <Text style={{textAlign:'center',marginTop:10,fontWeight:'medium',fontSize:20, textTransform: 'capitalize',}}> {item.strCategory}</Text>
                    </View>
                    
                    
                    </TouchableOpacity>
                  }
                  keyExtractor={({ id }, index) => index}
                  ItemSeparatorComponent={() => <View style={{margin: 10}}/>}
            />
          </View>
          <Gap height={25}/>
          <Text style={styles.textcategory}>New Foods</Text>
          <Gap height={10}/>
          <View>
            <FlatList 
                  data={this.state.data.meals}
                  renderItem={({ item }) =>
                    <TouchableOpacity  key={item.id} onPress={() => 
                  navigate('Detail' ,{
                      id : item.idMeal ,
                      judulnya : item.title,
                      img : item.poster_path,
                      overview : item.overview,
                      release_date : item.release_date,
                    })}>

                       <View style={styles.card}>
                         <View style={styles.cardImgWrapper}>
                          <Image
                            source={{ uri: `${item.strMealThumb}` }}
                            style={styles.cardImg}
                          />
                          </View>
                          <View style={styles.cardInfo}>
                            <Text style={styles.cardTitle}>{item.strMeal}</Text>
                            <Gap height={10} />
                            <Text style={styles.cardDetails}>
                            Category : {item.strCategory}
                            </Text>
                            <Text style={styles.cardDetails}>
                            From : {item.strArea}
                            </Text>
                            <Text style={styles.cardDetails}>
                            Tags : {item.strTags}
                            </Text>
                          </View>
                        </View>

                    </TouchableOpacity>
                  }
              keyExtractor={({ id }, index) => index}
              
            />
          </View>
        
      </ScrollView>
      </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  textcategory: {
    fontSize : 20,
    marginTop:10
  },
  container: { flex: 1,backgroundColor:'#FFFFFF'},
  flex: { flex: 1},
  header: {
		height: 112,
		borderRadius: 25,
		borderTopRightRadius: 0,
		borderTopLeftRadius: 0,
		flexDirection: 'row',
	},
	name: {
    marginTop: 7,
		fontSize: 20,
		fontWeight: 'bold',
    color:'black'
	},
	desc: {
    fontSize:18,
    marginTop: 25,
    color: '#4D4545',
    color:'black'
	},
  img2: {
    width: 88,
    height: 80,
    borderRadius: 40
  },
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center',
  },
  Image: {
    width: 120,
    height: 80,    
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 16,
  },
  textItemUrl: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 12,
    marginTop: 10,
    color: 'blue'
  },
  card: {
    height: 100,
    marginVertical: 10,
    flexDirection: 'row',
    shadowColor: '#999',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  cardImgWrapper: {
    flex: 1,
  },
  cardImg: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 8,
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
  },
  cardInfo: {
    flex: 2,
    padding: 10,
    borderColor: '#ccc',
    borderWidth: 1,
    borderLeftWidth: 0,
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: '#fff',
  },
  cardTitle: {
    fontWeight: 'bold',
  },
  cardDetails: {
    fontSize: 12,
    color: '#444',
  },
  img: { width: 75, height: 75, marginRight: 25, marginTop: 16 },
})