import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

const App = ({ navigation }) => {
	return(
		<View style={styles.container}>
        <Text style={styles.text}>Tentang Saya</Text>
         <Image  style={{width:150, height: 150, marginTop:20}}
          			source={require('../images/profile.png')}/>

        <Text style={styles.text2}>Ade Guntur Ramadhan</Text>
        <Text style={styles.text3}>React Native Developer</Text>
        <View style={styles.container2}>
            <Text style={{ marginTop:10,marginLeft:10,fontSize:15}}>Portofolio</Text>
            <View style={{ borderBottomColor: 'black', borderBottomWidth: 1,marginTop:5}}/>
            <View style={{flexDirection:'row',justifyContent:'space-around', alignItems: 'center'}}> 
                <View style={{justifyContent:'center', alignItems: 'center'}}>
                <Image  style={{width:50, height: 50, marginTop:20}}
          			source={require('../images/logo-gitlab.png')}/>
                <Text>@ade.guntur</Text>
                </View>
                <View style={{justifyContent:'center', alignItems: 'center'}}>
                <Image  style={{width:50, height: 50, marginTop:20}}
          			source={require('../images/vector.png')}/>
                <Text>@adeguntur</Text>
                </View>
            </View>
        </View>
        <View style={styles.container3}>
            <Text style={{ marginTop:10,marginLeft:10,fontSize:15}}>Hubungi Saya</Text>
            <View style={{ borderBottomColor: 'black', borderBottomWidth: 1,marginTop:5}}/>
            <View style={{flexDirection:'column',justifyContent:'space-around', alignItems: 'center'}}> 
                <View style={{flexDirection:'row',justifyContent:'center', alignItems: 'center'}}>
                <Image  style={{width:50, height: 50, marginTop:20}}
          			source={require('../images/facebook.png')}/>
                <Text style={{marginLeft:10,marginTop:20}}>Ade Guntur</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center', alignItems: 'center'}}>
                <Image  style={{width:50, height: 50, marginTop:20}}
          			source={require('../images/ig.png')}/>
                <Text style={{marginLeft:10,marginTop:20}}>@ade.guntur</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center', alignItems: 'center'}}>
                <Image  style={{width:50, height: 50, marginTop:20}}
          			source={require('../images/twitter.png')}/>
                <Text style={{marginLeft:10,marginTop:20}}>@uwuw123!!</Text>
                </View>
            </View>
        </View>
  		</View>
        
        
        );
};

export default App;

const styles = StyleSheet.create({
	container : {
	  flexGrow: 1,
	  justifyContent:'center',
	  alignItems: 'center'
	},
	container2 : {
	  width: 350,
	  height:150,
	  marginTop:20,
	  backgroundColor: '#EFEFEF',
	  borderRadius:25
	},
	container3 : {
	  width: 350,
	  height:260,
	  marginTop:20,
	  backgroundColor: '#EFEFEF',
	  borderRadius:25
	},
	text:{
	  marginBottom:10,
	  fontSize:30,
	  color: '#003366'
	},
	text2:{
	  marginTop:30,
	  fontSize:20,
	  color: '#003366'
	},
	text3:{
	  marginTop:10,
	  fontSize:15,
	  color: '#3EC6FF'
	},
	
  });