// Second screen which we will use to get the data
import React, { Component } from 'react';
//import react in our code.
import { StyleSheet, View, Text , FlatList, Image,ScrollView} from 'react-native';
//import all the components we are going to use.
import Gap from '../component/Gap'
import Axios from 'axios';
export default class DetailCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:'',
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getNewFood()
  }

    // Get Api Users
    getNewFood = async () => {
      try {
        const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${this.props.route.params.id}`)
        this.setState({ isError: false, isLoading: false, data: response.data })
        console.log("food", response.data)
      } catch (error) {
        this.setState({ isLoading: false, isError: true })
      }
    }
 

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
}
  render() {
    // const { navigate } = this.props.navigation;
    let ade = this.state.data.meals;
   
    return (
      //View to hold our multiple components
      <View style={styles.container}>
      <ScrollView>
      <View style={{height:1000}}>
      <Image source={{ uri: `${this.props.route.params.strCategoryThumb}` }} style={styles.img} />
        <Gap height={39}/>
        <View style={{paddingLeft:20}}>
          <Text style={styles.TextStyle}>{this.props.route.params.strCategory}</Text>
          <Gap height={25}/>
          <Text style={{fontSize:15}}>Description</Text>
          <Gap height={10}/>
          <Text style={styles.overview}>{this.props.route.params.strCategoryDescription}</Text>
          <Gap height={20}/>
          
        </View>
        </View>
      </ScrollView>
      </View>
    );
    
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  release:{
    fontSize : 13
  },
  img: {
    width : '100%',
    height : '35%'
  },
  TextStyle: {
    fontSize: 22,
    color: '#2C1F62',
    fontWeight : 'bold'
  },
  overview: {
    fontSize: 16,
    color: 'black',
    paddingRight : 20
  },
});