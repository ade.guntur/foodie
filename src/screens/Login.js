import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View , Alert,TouchableOpacity} from 'react-native';

export default class Login extends Component{
	
	constructor(props) {
		//constructor to set default state
		super(props);
		this.state = {
			username: '',
			password: '',
			isError: false,
		};
	  }

	  load() {
		  if(this.state.username == "" && this.state.password == ""){
			Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
		  }else if(this.state.username == "" || this.state.password == ""){
			Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
		  }
		  else {
			this.props.navigation.navigate('Home' ,  {username: this.state.username})
		  }
	  }
	render(){
		const { navigate } = this.props.navigation;
		return (
		<View style={styles.container}>
        <Text style={styles.logo}>Foodies</Text>

		<Text style={styles.logo2}>Made with loves</Text>

		<TouchableOpacity
            style={[styles.signIn, {
                        borderColor: '#FF6347',
                        borderWidth: 1,
                        marginTop: 15,
						borderRadius:25,
						width:"80%",
                   }]}
                >
        <TextInput  
            style={styles.inputText}
			value={this.state.username}  
						onChangeText={username => this.setState({ username })}
          				placeholder={'Enter Username'}/>
      </TouchableOpacity>

		<TouchableOpacity
            style={{
                        borderColor: '#FF6347',
                        borderWidth: 1,
                        marginTop: 15,
						borderRadius:25,
						width:"80%",
                   }}
                >
         <TextInput  
            secureTextEntry
            style={styles.inputText}
			value={this.state.password}  
				   onChangeText={password => this.setState({ password })}
          		   placeholder={'Enter Password'}/>
     	</TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.loginBtn}onPress={() => this.load()}>
          <Text style={styles.loginText} >LOGIN</Text>
        </TouchableOpacity>
		
      </View>
		);
		}
}

const styles = StyleSheet.create({
	container: {
	  flex: 1,
	  backgroundColor: '#ffffff',
	  alignItems: 'center',
	  justifyContent: 'center',
	},
	logo:{
	  fontWeight:"bold",
	  fontSize:50,
	  color:"#fb5b5a",
	  marginBottom:10
	},
	logo2:{
		fontWeight:"bold",
		fontSize:15,
		color:"#fb5b5a",
		marginBottom:40
	  },
	inputView:{
	  width:"80%",
	  borderColor:'#1BA0E1',
	  borderRadius:25,
	  height:50,
	  marginBottom:20,
	  justifyContent:"center",
	  padding:20
	},
	inputText:{
	  paddingLeft:20,
	  height:50,
	},
	forgot:{
	  color:"white",
	  fontSize:11
	},
	loginBtn:{
	  width:"80%",
	  backgroundColor:"#fb5b5a",
	  borderRadius:25,
	  height:50,
	  alignItems:"center",
	  justifyContent:"center",
	  marginTop:40,
	  marginBottom:10
	},
	signupBtn:{
		width:"80%",
		borderColor: '#fb5b5a',
		borderRadius:25,
		height:50,
		alignItems:"center",
		justifyContent:"center",
		marginTop:10,
		marginBottom:10
	  },
	loginText:{
	  color:"white"
	}
  });