// Second screen which we will use to get the data
import React, { Component } from 'react';
//import react in our code.
import { StyleSheet, View, Text , FlatList, Image,ScrollView} from 'react-native';
//import all the components we are going to use.
import Gap from '../component/Gap'
import Axios from 'axios';
export default class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:'',
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getNewFood()
  }

    // Get Api Users
    getNewFood = async () => {
      try {
        const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${this.props.route.params.id}`)
        this.setState({ isError: false, isLoading: false, data: response.data })
        console.log("food", response.data)
      } catch (error) {
        this.setState({ isLoading: false, isError: true })
      }
    }
 

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
}
  render() {
    // const { navigate } = this.props.navigation;
    let ade = this.state.data.meals;
    if (typeof ade !== 'undefined'){
      let datas = ade[0]
      return (
        //View to hold our multiple components
        <View style={styles.container}>
        <ScrollView>
        <View style={{height:1000}}>
        <Image source={{ uri: `${datas.strMealThumb}` }} style={styles.img} />
          <Gap height={39}/>
          <View style={{paddingLeft:20}}>
            <Text style={styles.TextStyle}>{datas.strMeal}</Text>
            <Gap height={25}/>
            <Text style={styles.release}>From : {datas.strArea}</Text>
            <Gap height={8}/>
            <Text style={styles.release}>Tags : {datas.strTags}</Text>
            <Gap height={25}/>
            <Text style={{fontSize:15}}>Instructions</Text>
            <Gap height={10}/>
            <Text style={styles.overview}>{datas.strInstructions}</Text>
            <Gap height={20}/>
            
          </View>
          </View>
        </ScrollView>
        </View>
      );
    }
    else{
      return (
        //View to hold our multiple components
        <View style={styles.container}>
         {/* <Image source={{ uri: `${asd}` }} style={styles.img} /> */}
          <Gap height={39}/>
          <View style={{paddingLeft:20}}>
            <Gap height={25}/>
            <Text>Still Loading...</Text>
            <Gap height={10}/>
      
          </View>
        </View>
      );
    }
    
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  release:{
    fontSize : 13
  },
  img: {
    width : '100%',
    height : '35%'
  },
  TextStyle: {
    fontSize: 22,
    color: '#2C1F62',
    fontWeight : 'bold'
  },
  overview: {
    fontSize: 16,
    color: 'black',
    paddingRight : 20
  },
});